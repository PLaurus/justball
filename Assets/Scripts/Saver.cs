﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public static class Saver{
    /*public SoundSwitcher soundSwitcher;
    public Text bestScoreText;
    public Text coinsText;
    public Shop shop;
    public Bonus bonus;*/
    private static string path;

    public static void Save()
    {
        path = Application.persistentDataPath + "/SaveFile.jb";
        BinaryFormatter binary = new BinaryFormatter();
        FileStream fStream = File.Create(path);
        SaveManager.GetInstance().SetValues(Data.bestScore, Data.sound, Data.coinsCount, Data.balls,Data.rewardPreviousDay, Data.todayCoinsBonus);
        binary.Serialize(fStream, SaveManager.GetInstance());
        fStream.Close();
    }

    public static void Load()
    {
        path = Application.persistentDataPath + "/SaveFile.jb";
        Debug.Log(path);
        if (File.Exists(path))
        {
            BinaryFormatter binary = new BinaryFormatter();
            FileStream fStream = File.Open(path, FileMode.Open);
            SaveManager.RecoverInstance((SaveManager)binary.Deserialize(fStream));
            fStream.Close();
            Data.bestScore = SaveManager.GetInstance().bestScore;
            //bestScoreText.text = Data.bestScore.ToString();
            Data.coinsCount = SaveManager.GetInstance().coins;
            //coinsText.text = SaveManager.GetInstance().coins.ToString();
            Data.balls = SaveManager.GetInstance().GetBalls();
            //shop.SecondLaunch();
            //bonus.SetPreviousDay(SaveManager.GetInstance().rewardPreviousDay);
            Data.rewardPreviousDay = SaveManager.GetInstance().rewardPreviousDay;
            //bonus.SetCoinsBonus(SaveManager.GetInstance().todayBonusCoins);
            Data.todayCoinsBonus = SaveManager.GetInstance().todayBonusCoins;
            Data.sound = SaveManager.GetInstance().sound;

            Data.firstLaunchMenu = false;
            if(Data.balls.Count != 0) Data.firstLaunchShop = false;
            /*if (Data.sound)
                soundSwitcher.SwitchSoundOn();
            else
                soundSwitcher.SwitchSoundOff();*/
        }
        else
        {
            Data.coinsCount = 10;
            //coinsText.text = Data.coinsCount.ToString();
            //shop.firstLaunch();
            //bonus.SetPreviousDay(new DateTime(2017, 08, 17));
            Data.rewardPreviousDay = new DateTime(2017, 08, 17);
            //Data.firstLaunch = false;
        }
    }
}

[Serializable]
class SaveManager
{
    private static SaveManager saveManager;

    public DateTime rewardPreviousDay { get; private set; }
    public uint todayBonusCoins { get; private set; }
    public uint bestScore { get; private set; }
    public bool sound { get; private set; }
    public uint coins { get; private set; }
    private List<Ball> balls;

    private SaveManager()
    {
        rewardPreviousDay = new DateTime(2017, 08, 17);
        todayBonusCoins = 0;
        coins = 10;
        bestScore = 0;
        sound = true;
        balls = new List<Ball>();
    }

    public static SaveManager GetInstance()
    {
        if (saveManager == null)
        {
            saveManager = new SaveManager();
        }
        return saveManager;
    }

    public static void RecoverInstance(SaveManager serializedInstanse)
    {
        if (saveManager != null)
        {
            return;
        }
        saveManager = serializedInstanse;
    }

    public void SetValues(uint bestScore, bool sound, uint coins, List<Ball> balls,
        DateTime rewardPreviousDay, uint todayBonusCoins)
    {

        this.bestScore = bestScore;
        this.sound = sound;
        this.coins = coins;
        this.balls = balls;
        this.rewardPreviousDay = rewardPreviousDay;
        this.todayBonusCoins = todayBonusCoins;

    }

    public List<Ball> GetBalls()
    {
        return balls;
    }
}
