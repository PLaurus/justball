﻿using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class GameSocials{

    public static string leaderboard = "CgkIjKntmcwfEAIQCw";
    public static string achievement1 = "CgkIjKntmcwfEAIQAQ";//Coins hunter.
    public static string achievement2 = "CgkIjKntmcwfEAIQAg";//Treasure hunter.
    public static string achievement3 = "CgkIjKntmcwfEAIQAw";//Pathfinder.
    public static string achievement4 = "CgkIjKntmcwfEAIQBA";//Experienced adventurer.
    public static string achievement5 = "CgkIjKntmcwfEAIQBQ";//Rich ball.
    public static string achievement6 = "CgkIjKntmcwfEAIQBg";//Money bag.
    public static string achievement7 = "CgkIjKntmcwfEAIQBw";//Not quite what was planned.
    public static string achievement8 = "CgkIjKntmcwfEAIQCA";//Persistent ball.
    public static string achievement9 = "CgkIjKntmcwfEAIQCQ";//I can open my own shop now!!!
    public static string achievement10 = "CgkIjKntmcwfEAIQCg";//It's unbelievable!
    

    public static void Authenticate(GameObject achievementsButton, GameObject leaderBoardButton)
    {
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) => {
            if (success)
            {
                Debug.Log("Authenticated successfuly");
                achievementsButton.GetComponent<Image>().color = new Color(1f, 1f, 1f);
                achievementsButton.GetComponent<Image>().raycastTarget = true;
                leaderBoardButton.GetComponent<Image>().color = new Color(1f, 1f, 1f);
                leaderBoardButton.GetComponent<Image>().raycastTarget = true;
            }
            else
            {
                achievementsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);
                achievementsButton.GetComponent<Image>().raycastTarget = false;
                leaderBoardButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);
                leaderBoardButton.GetComponent<Image>().raycastTarget = false;
            }
        });
    }

    public static bool IsAuthenticated()
    {
        return Social.localUser.authenticated;
    }

    public static void UnlockAchivement(string achievement)
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportProgress(achievement, 100.0f, (bool success) => {
                if (success)
                {
                    Debug.Log("Achievement successfully obtained");
                }
                else
                {
                    Debug.Log("Achievement obtaining failed");
                }
            });
        }
    }

    public static void IncrementAchievement(string achievement)
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(achievement, 1, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Achievement successfully incremented");
                }
                else
                {
                    Debug.Log("Achievement incrementing failed");
                }
            });
        }
    }

    public static void IncrementAchievement(string achievement, int step)
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(achievement, step, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Achievement successfully incremented");
                }
                else
                {
                    Debug.Log("Achievement incrementing failed");
                }
            });
        }
    }

    public static void ReportScore(int score)
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(score, leaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Score successfully reported");
                }
                else
                {
                    Debug.Log("Score report failed");
                }
            });
        }
    }

    public static void ShowLeaderBoard()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboard);
            //IncrementAchievement(achievement9);
        }
        else
        {
            Debug.Log("Вы не авторизованы!");
            //IncrementAchievement(achievement9);
        }
    }

    public static void ShowAchievements()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
            //IncrementAchievement(achievement10);
        }
        else
        {
            Debug.Log("Вы не авторизованы!");
            //IncrementAchievement(achievement10);
        }
    }

    public static void SignOut()
    {
        //((PlayGamesPlatform)Social.Active).SignOut();
        PlayGamesPlatform.Instance.SignOut();
    }
}
