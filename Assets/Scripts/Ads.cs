﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System;
using UnityEngine.UI;

public class Ads : MonoBehaviour {
     
    private const string banner1 = "ca-app-pub-3007395738037111/9944073324";
    private const string banner2 = "ca-app-pub-3007395738037111/6222775575";
    private const string rewardVideo = "ca-app-pub-3007395738037111/5483945954";
    private BannerView bannerView;
    private AdRequest request;
    private RewardBasedVideoAd rewardBasedVideoAd;
    public Text coinsText;//,test;
    public GameObject rewardButton;

    void Start()
    {
        rewardBasedVideoAd = RewardBasedVideoAd.Instance;
        rewardBasedVideoAd.OnAdRewarded += HandleOnAdRewarded;
        rewardBasedVideoAd.OnAdLoaded += HandleOnAdLoaded;
        rewardBasedVideoAd.OnAdClosed += HandleOnAdClosed;
        rewardBasedVideoAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;

        if (!Data.allGameStarted)
        {
            bannerView = new BannerView(banner1, AdSize.SmartBanner, AdPosition.Bottom);
            request = new AdRequest.Builder().Build();
            bannerView.LoadAd(request);

            rewardButton.SetActive(false);
            LoadRewardVideoAd();
            Data.allGameStarted = true;
        }
        else
        {
            /*if (rewardBasedVideoAd.IsLoaded())
            {
                rewardButton.SetActive(true);
            }
            else
            {*/
                rewardButton.SetActive(false);
                LoadRewardVideoAd();
            //}
        }
    }

    void OnDestroy()
    {
        rewardBasedVideoAd.OnAdRewarded -= HandleOnAdRewarded;
        rewardBasedVideoAd.OnAdLoaded -= HandleOnAdLoaded;
        rewardBasedVideoAd.OnAdClosed -= HandleOnAdClosed;
        rewardBasedVideoAd.OnAdFailedToLoad -= HandleOnAdFailedToLoad;
    }

    private void LoadRewardVideoAd()
    {
        //test.text += "Началась загрузка видоса";
        rewardBasedVideoAd.LoadAd(new AdRequest.Builder().Build(), rewardVideo);
    }

    public void ShowRewardVideoAd()
    {
        if (rewardBasedVideoAd.IsLoaded())
        {
            rewardBasedVideoAd.Show();
            rewardButton.SetActive(false);
            //test.text += "Показали видео";
        }
    }

    public void  HandleOnAdLoaded(object sender, EventArgs args)
    {
        //test.text += "Видео загружено";
        rewardButton.SetActive(true);

    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //test.text = args.Message;
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        //test.text += "Видос закрыт";
        LoadRewardVideoAd();
    }

    public void HandleOnAdRewarded(object sender, Reward args)
    {
        //test.text += "Получена награда";
        Data.coinsCount += (uint)args.Amount;
        //test.text += "Data.coinsCount += (uint)args.Amount;";
        coinsText.text = Data.coinsCount.ToString();
        //test.text += "coinsText.text = "+ Data.coinsCount;
        Saver.Save();
        LoadRewardVideoAd();
    }

    /*public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {

    }*/
}
