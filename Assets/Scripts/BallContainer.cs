﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallContainer : MonoBehaviour {
    public Image ballImage;
    public GameObject lockGO;
    public Text cost;
    public GameObject costBackground;
    public Text coinsMultText;
    public Text forceMultText;
    public GameObject coinsMultContainer;
    public GameObject forceMultContainer;


    private int id = 0;

    public void SetId(int id)
    {
        this.id = id;
    }

    public void Click()
    {
        GameObject.Find("Canvas").GetComponent<Shop>().ChooseBall(id);
    }
}
