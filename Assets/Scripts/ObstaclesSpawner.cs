﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpawner : MonoBehaviour {
    private const int CLONE_COUNT = 2;
    //private int hardStage = 0; // стадия сложности. от нее завият препятствия(пока не используется)

    public GameObject[] twoStartObstacles = new GameObject[2];
    public GameObject[] obstacles = new GameObject[4];
    public GameObject lastObstacle;

    private GameObject fixedLastObstacle;
    private GameObject[,] allObstacles;
    private float S;
    private float screenBottomBorderPos;
    private float obstacleBottomBorderPos;
    private bool obstaclesReadyToSpawn = false;
    private int newObstacleId;
    private Vector3 pos;
    private Vector3 disabledObjsPos;

    void Start()
    {
        fixedLastObstacle = lastObstacle;
        allObstacles = new GameObject[obstacles.Length, CLONE_COUNT];
        disabledObjsPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 2 * Screen.height));
        for (int i = 0; i < obstacles.Length; i++)
        {
            for (int j = 0; j < CLONE_COUNT; j++)
            {
                allObstacles[i, j] = Instantiate(obstacles[i], new Vector3(disabledObjsPos.x, disabledObjsPos.y, obstacles[i].transform.position.z),
                    Quaternion.identity, transform) as GameObject;
                allObstacles[i, j].GetComponent<ObjsActivitySwitcher>().setIds(i, j);
            }
        }
        setS();
        obstacleBottomBorderPos = lastObstacle.GetComponent<SpriteRenderer>().transform.position.y -
            lastObstacle.GetComponent<SpriteRenderer>().bounds.extents.y;
    }

    void Update()
    {

        /*Vector3 start = new Vector3(0f, twoStartObstacles[0].GetComponent<SpriteRenderer>().transform.position.y - twoStartObstacles[0].GetComponent<SpriteRenderer>().bounds.extents.y);

        Vector3 finish = new Vector3(0f, twoStartObstacles[1].GetComponent<SpriteRenderer>().transform.position.y - twoStartObstacles[1].GetComponent<SpriteRenderer>().bounds.extents.y);
        Debug.DrawLine(start, finish);*/
        if (Data.curScene == Data.Scene.GAME)
        {
            screenBottomBorderPos = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f, 0f)).y;
            GenerateNewId();
            if ((screenBottomBorderPos - obstacleBottomBorderPos) < 0)
            {
                float NextObstacleHalfScale = obstacles[newObstacleId].GetComponent<SpriteRenderer>().bounds.extents.y;
                pos = new Vector3(obstacles[newObstacleId].transform.position.x, obstacleBottomBorderPos - S - NextObstacleHalfScale,
                    obstacles[newObstacleId].transform.position.z);
                spawn();
            }
        }
    }

    void GenerateNewId()
    {
        if (!obstaclesReadyToSpawn)
        {
            newObstacleId = Random.Range(0, obstacles.Length);
            obstaclesReadyToSpawn = true;
        }
    }

    void GenerateNewId(int exceptThisId)
    {
        if (exceptThisId >= 1)
        {
            newObstacleId--;
        }
        else if (exceptThisId == 0)
        {
            newObstacleId++;
        }
    }

    void spawn()
    {
        for (int i = 0; i < CLONE_COUNT; i++)
        {
            if (!allObstacles[newObstacleId, i].activeInHierarchy)
            {
                allObstacles[newObstacleId, i].transform.position = pos;
                allObstacles[newObstacleId, i].GetComponent<ObjsActivitySwitcher>().setBottomBorderPos();

                foreach (Transform go in allObstacles[newObstacleId, i].GetComponentsInChildren<Transform>(true))
                {
                    if (go.gameObject.CompareTag("Coin"))
                        go.gameObject.SetActive(true);
                }
                if (newObstacleId == 1)
                {
                    allObstacles[newObstacleId, i].SetActive(true);
                }
                else allObstacles[newObstacleId, i].SetActive(true);
               
                //Debug.Log("Позиция нового обекта:"+ allObstacles[newObstacleId, i].transform.position);
                lastObstacle = allObstacles[newObstacleId, i];
                obstacleBottomBorderPos = lastObstacle.GetComponent<SpriteRenderer>().transform.position.y -
                lastObstacle.GetComponent<SpriteRenderer>().bounds.extents.y;
                obstaclesReadyToSpawn = false;
                break;
            }
            if (i == (CLONE_COUNT - 1))
            {
                GenerateNewId(newObstacleId);
                spawn();
            }
        }
    }

    void setS()
    {
        S = twoStartObstacles[0].GetComponent<SpriteRenderer>().transform.position.y - twoStartObstacles[1].GetComponent<SpriteRenderer>().transform.position.y
            - twoStartObstacles[0].GetComponent<SpriteRenderer>().bounds.extents.y - twoStartObstacles[1].transform.GetComponent<SpriteRenderer>().bounds.extents.y;
       
    }

    public float GetS()
    {
        return S;
    }

    public Vector3 getDisabledObjsPos()
    {
        return disabledObjsPos;
    }

    public void disablePlat(int i, int j)
    {
        allObstacles[i, j].SetActive(false);
    }

    public void Reset()
    {
        for (int i = 0; i < obstacles.Length; i++)
        {
            for (int j = 0; j < CLONE_COUNT; j++)
            {
                allObstacles[i, j].SetActive(false);
                allObstacles[i, j].transform.position = disabledObjsPos;
            }
        }
        lastObstacle = fixedLastObstacle;
        obstacleBottomBorderPos = lastObstacle.GetComponent<SpriteRenderer>().transform.position.y -
            lastObstacle.GetComponent<SpriteRenderer>().bounds.extents.y;
    }
}
