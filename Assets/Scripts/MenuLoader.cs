﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuLoader : MonoBehaviour {
    public Text bestScoreText;
    public Text coinsText;
    //public Shop shop;
    public Bonus bonus;
    public SoundSwitcher soundSwitcher;
    public GameObject achievementButton;
    public GameObject leaderBoardButton;
    //public Text test;

    public Sprite[] ballsSprites = new Sprite[8];
    private void Awake()
    {
        Saver.Load();
        Data.curScene = Data.Scene.MENU;
        foreach(Ball ball in Data.balls)
        {
            if(ball.selected)
            {
                Data.characterSprite = ballsSprites[ball.id];
                Data.coinMultiplier = ball.coinsMultiplier;
                Data.forceMultiplier = ball.forceMultiplier;
                break;
            }
        }
        bestScoreText.text = Data.bestScore.ToString();
        coinsText.text = SaveManager.GetInstance().coins.ToString();
        if (Data.firstLaunchMenu)
        {
            //shop.firstLaunch();
            bonus.SetPreviousDay(new DateTime(2018, 01, 10));
        }
        else
        {
            //shop.SecondLaunch();
            bonus.SetPreviousDay(Data.rewardPreviousDay);
            bonus.SetCoinsBonus(Data.todayCoinsBonus);
            soundSwitcher.InitButton();
            /*foreach(Ball ball in Data.balls)
            {
                if(ball.selected) Data.characterSprite = ball.s
            }*/
        }
        if (!GameSocials.IsAuthenticated())
        {
            GameSocials.Authenticate(achievementButton, leaderBoardButton);
        }
        else
        {
            achievementButton.GetComponent<Image>().color = new Color(1f, 1f, 1f);
            achievementButton.GetComponent<Image>().raycastTarget = true;
            leaderBoardButton.GetComponent<Image>().color = new Color(1f, 1f, 1f);
            leaderBoardButton.GetComponent<Image>().raycastTarget = true;
        }

    }

    public void ShowLeaderBoard()
    {
        GameSocials.ShowLeaderBoard();
    }

    public void ShowAchievements()
    {
        GameSocials.ShowAchievements();
    }
}
