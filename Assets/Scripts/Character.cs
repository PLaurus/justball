﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    private const float FORCE = 100.0f;
    //private float forceMultiplier = 1f;

    public Text coinsText;
    public AudioSource coinSound;
    //private uint multiplier = 1;
    private float screenCenter = 0f;

    void Start () {
        Data.character = this.transform;
        screenCenter = Screen.width / 2;
    }
	
	
	void FixedUpdate () {
        Move();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        ObtainCoin(other);
    }

    private void Move()
    {
        if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(-Time.fixedDeltaTime * FORCE * Data.forceMultiplier, 0));
        }
        else if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Time.fixedDeltaTime * FORCE * Data.forceMultiplier, 0));
        }
        if(Input.touchCount == 1)
        {
            if (Input.GetTouch(0).position.x < screenCenter)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(-Time.fixedDeltaTime * FORCE * Data.forceMultiplier, 0));
            }
            else if (Input.GetTouch(0).position.x >= screenCenter)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(Time.fixedDeltaTime * FORCE * Data.forceMultiplier, 0));
            }
        }
    }

    private void ObtainCoin(Collider2D other)
    {
        if (other.gameObject.tag == "Coin")
        {
            other.gameObject.SetActive(false);
            Data.coinsCount += 1 * (uint)Data.coinMultiplier;
            Data.obtainedCoins += 1 * (uint)Data.coinMultiplier;
            coinsText.text = Data.obtainedCoins.ToString();
            if(Data.sound) coinSound.Play();
            if (Data.obtainedCoins >= 50) GameSocials.UnlockAchivement(GameSocials.achievement1);
            else if (Data.obtainedCoins >= 200) GameSocials.UnlockAchivement(GameSocials.achievement2);
        }
    }

    public void SetSprite(Sprite sprite)
    {
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

}
