﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[RequireComponent(typeof(BoxCollider2D))]
public class Loose : MonoBehaviour {
    
    public GameObject[] gameUiElements = new GameObject[3];
    public Text scoreText, scoreValue;
    public GameObject resultWindow;

    //public RectTransform pauseRectTransform;
    //public GameObject looseText;
    //public GameObject highScoreGameObject;
    //public GameSocials achievements;
    //public Text highScoreText, highScoreTextInfo;
    //public AdBanner adBanner;

    /*const uint achievementScore1 = 100;
    const uint achievementScore2 = 1000;
    const uint achievementScore3 = 5000;
    const uint achievementScore4 = 333;
    const uint achievementScore5 = 1001;*/
    private Vector2 deathPosition = Vector2.zero;
    //private SpriteRenderer sawsSpriteRenderer;


    void Start()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameOver();
        }
    }

    void MoveCharacterToSaws()
    {
        deathPosition.x = transform.position.x;
        deathPosition.y = transform.position.y;
        Data.character.position = new Vector3(deathPosition.x, deathPosition.y, Data.character.position.z);
        Data.character.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Data.character.GetComponent<Rigidbody2D>().isKinematic = true;
    }

    void GameOver()
    {
        if (Data.curScene == Data.Scene.GAME)
        {
            MoveCharacterToSaws();
            foreach(GameObject go in gameUiElements)
            {
                go.SetActive(false);
            }
            Data.curScene = Data.Scene.RESULT_WINDOW;
            /*if (curScore >= achievementScore1)
            {
                achievements.UnlockAchivement(achievements.achievement1);
            }
            if (curScore >= achievementScore2)
            {
                achievements.UnlockAchivement(achievements.achievement2);
            }
            if (curScore >= achievementScore3)
            {
                achievements.UnlockAchivement(achievements.achievement3);
            }
            if (curScore == achievementScore4)
            {
                achievements.UnlockAchivement(achievements.achievement4);
            }
            else if (curScore == achievementScore5)
            {
                achievements.UnlockAchivement(achievements.achievement5);
            }*/

            if (Data.curScore > Data.bestScore)
            {
                Data.bestScore = Data.curScore;
                scoreText.text = "New Best Score!";
                scoreValue.text = Data.bestScore.ToString();
                GameSocials.ReportScore((int)Data.bestScore);
            }
            else
            {
                scoreText.text = "Your Score";
                scoreValue.text = Data.curScore.ToString();
            }
            resultWindow.SetActive(true);
            GameSocials.IncrementAchievement(GameSocials.achievement5, (int)Data.obtainedCoins);
            GameSocials.IncrementAchievement(GameSocials.achievement6, (int)Data.obtainedCoins);
            GameSocials.IncrementAchievement(GameSocials.achievement7);
            GameSocials.IncrementAchievement(GameSocials.achievement8);
            Saver.Save();

            //achievements.IncrementAchievement(achievements.achievement11, Convert.ToInt32(curScore));
            //achievements.ReportScore(Convert.ToInt32(curScore));
            //adBanner.ShowVideoBanner();// реклама во все ебало
                                       
        }
    }
}
