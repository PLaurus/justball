﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public Text scoreText;
    private float lastPos;

    void Start()
    {
        lastPos = 0f;
    }

    public void UpdateScore()
    {
        if (Data.character.position.y < lastPos && Data.character.position.y < 0f)
        {
            lastPos = Data.character.position.y;
            Data.curScore = (uint)Mathf.Abs(Data.character.position.y);
            scoreText.text = Data.curScore.ToString();
            if (Data.curScore >= 15) GameSocials.UnlockAchivement(GameSocials.achievement3);
            else if (Data.curScore >= 100) GameSocials.UnlockAchivement(GameSocials.achievement4);
            else if (Data.curScore >= 1000) GameSocials.UnlockAchivement(GameSocials.achievement10);
        }
    }

    public void ResetScore()
    {
        lastPos = 0f;
        Data.curScore = 0;
        scoreText.text = "0";
    }
}
