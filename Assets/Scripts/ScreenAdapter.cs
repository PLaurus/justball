﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenAdapter : MonoBehaviour {
    //public GameObject background;
    private const float firstSize = 5f;
    private const float firstHeight = 1280f;
    private const float firstWidth = 800f;
    void Start () {

        float firstRatio = firstHeight / firstWidth;
        float ratio = (float)Screen.height / Screen.width;
        float size = ratio * firstSize / firstRatio;
        Camera.main.orthographicSize = size;
        //Debug.Log(background.transform.localScale.x);
    }
	
	void Update () {
		
	}
}
