﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bonus : MonoBehaviour {

    public GameObject bonusBox;
    public Text bonusCoinsText;
    public Text coinsText;
    //public Saver saver;

    private const int mondayBonus = 50;
    private const int tuesdayBonus = 55;
    private const int wednesdayBonus = 60;
    private const int thursdayBonus = 70;
    private const int fridayBonus = 75;
    private const int saturdayBonus = 100;
    private const int sundayBonus = 110;
    private const int range = 10;

    //public DateTime previousDay { get; private set; }
    //public uint todayCoinsBonus { get; private set; }

    private DateTime currentDay;

    void Start()
    {
        //previousDay = Data.rewardPreviousDay;
        //Data.todayCoinsBonus = Data.todayCoinsBonus;
        currentDay = DateTime.Today;
        if (currentDay > Data.rewardPreviousDay)
        {
            Data.todayCoinsBonus = 0;
            if (currentDay.DayOfWeek == DayOfWeek.Monday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(mondayBonus - range, mondayBonus + range);
            }
            else if (currentDay.DayOfWeek == DayOfWeek.Tuesday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(tuesdayBonus - range, tuesdayBonus + range);
            }
            else if (currentDay.DayOfWeek == DayOfWeek.Wednesday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(wednesdayBonus - range, wednesdayBonus + range);
            }
            else if (currentDay.DayOfWeek == DayOfWeek.Thursday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(thursdayBonus - range, thursdayBonus + range);
            }
            else if (currentDay.DayOfWeek == DayOfWeek.Friday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(fridayBonus - range, fridayBonus + range);
            }
            else if (currentDay.DayOfWeek == DayOfWeek.Saturday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(saturdayBonus - range, saturdayBonus + range);
            }
            else if (currentDay.DayOfWeek == DayOfWeek.Sunday)
            {
                Data.todayCoinsBonus += (uint)UnityEngine.Random.Range(sundayBonus - range, sundayBonus + range);
            }
            Debug.Log("Сегодняшний бонус(" + currentDay.DayOfWeek + "): " + Data.todayCoinsBonus);
            Data.rewardPreviousDay = currentDay;
            Saver.Save();
        }
        else
        {
            Debug.Log("Бонус уже сгенерирован!");
        }
        if (Data.todayCoinsBonus > 0)
        {
            bonusCoinsText.text = Data.todayCoinsBonus.ToString();
            bonusBox.SetActive(true);
        }
    }

    public void ObtainBonus()
    {
        Data.coinsCount += Data.todayCoinsBonus;
        coinsText.text = Data.coinsCount.ToString();
        Data.todayCoinsBonus = 0;
        bonusBox.SetActive(false);
        Saver.Save();
    }

    public void SetPreviousDay(DateTime data)
    {
        Data.rewardPreviousDay = data;
    }

    public void SetCoinsBonus(uint newCoinsBonus)
    {
        Data.todayCoinsBonus = newCoinsBonus;
    }
}
