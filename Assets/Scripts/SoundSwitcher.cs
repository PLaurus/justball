﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSwitcher : MonoBehaviour {

    //public GameObject soundOnIcon;
    //public GameObject soundOffIcon;
    public Sprite soundOn, soundOff;
    public AudioSource[] audioSource = new AudioSource[1];
    //public Saver saver;

    public void InitButton()
    {
        foreach (AudioSource audio in audioSource)
        {
            audio.mute = !Data.sound;
        }
        if (Data.sound) GetComponent<Image>().sprite = soundOn;
        else GetComponent<Image>().sprite = soundOff;
    }


    public void SwitchSound()
    {
        if (Data.sound)
        {
            foreach (AudioSource audio in audioSource)
            {
                audio.mute = true;
            }
            GetComponent<Image>().sprite = soundOff;
            Data.sound = false;
           
        }
        else{
            foreach (AudioSource audio in audioSource)
            {
                audio.mute = false;
            }
            GetComponent<Image>().sprite = soundOn;
            Data.sound = true;
        }
        Saver.Save();
    }
}
