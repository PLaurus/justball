﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjsActivitySwitcher : MonoBehaviour {

    private ObstaclesSpawner obstaclesSpawner;

    private const float sMultiplier = 1f;

    public bool startObject = false;

    private int platId, cloneId;
    private float screenUpperBorderPos;
    private float obstacleBottomBorderPos;

    void Start()
    {
        obstaclesSpawner = GameObject.FindGameObjectWithTag("GameEnvirorment").GetComponent<ObstaclesSpawner> ();
        setBottomBorderPos();
    }

    void Update()
    {
        if (Data.curScene == Data.Scene.GAME)
        {
            screenUpperBorderPos = Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f)).y;
            if ((obstacleBottomBorderPos - screenUpperBorderPos) > obstaclesSpawner.GetS() * sMultiplier)
            {
                if (startObject)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    transform.position = obstaclesSpawner.getDisabledObjsPos();
                    obstaclesSpawner.disablePlat(platId, cloneId);
                }
            }
        }
    }

    public void setIds(int platId, int cloneId)
    {
        this.platId = platId;
        this.cloneId = cloneId;
    }

    public void setBottomBorderPos()
    {
        obstacleBottomBorderPos = GetComponent<SpriteRenderer>().transform.position.y -
            GetComponent<SpriteRenderer>().bounds.extents.y;
    }
}
