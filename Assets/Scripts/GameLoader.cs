﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour {
    public Character character;
    public Sprite defaultSprite;
    public GameObject resultWindow;
    public GameObject[] gameUiElements = new GameObject[3];
    public GameObject startObjs;
    public CameraPosController cameraPosController;
    public ObstaclesSpawner obstaclesSpawner;
    public Score score;
    public SawsMoving sawsMoving;
    public Text coinsText;
    public GameObject pauseWindow;

    public Text coinsMultText;
    public Text forceMultText;
    public GameObject coinsMultContainer;
    public GameObject forceMultContainer;

    private Vector3 startObjsPos;
    private Vector3 startCharacterPos;
    private void Awake()
    {
        if (Data.characterSprite == null) Data.characterSprite = defaultSprite;
        character.SetSprite(Data.characterSprite);

        if (Data.coinMultiplier > 1)
        {
            coinsMultContainer.SetActive(true);
            coinsMultText.text = "x" + Data.coinMultiplier.ToString();
        }
        else
        {
            coinsMultContainer.SetActive(false);
        }

        if (Data.forceMultiplier > 1)
        {
            forceMultContainer.SetActive(true);
            forceMultText.text = "x" + Data.forceMultiplier.ToString();
        }
        else
        {
            forceMultContainer.SetActive(false);
        }

        Data.curScene = Data.Scene.GAME;
    }

    void Start()
    {
        startObjsPos = startObjs.transform.position;
        startCharacterPos = character.transform.position;
        StartGame();
    }

    public void StartGame()
    {
        score.ResetScore();
        Data.obtainedCoins = 0;
        coinsText.text = Data.obtainedCoins.ToString();
    }

    public void RestartGame()
    {
        resultWindow.gameObject.SetActive(false);
        pauseWindow.gameObject.SetActive(false);
        foreach (GameObject go in gameUiElements)
        {
            go.SetActive(true);
        }
        startObjs.transform.position = startObjsPos;
        startObjs.gameObject.SetActive(true);
        cameraPosController.ResetCameraPos();
        cameraPosController.ResetCheckPoints();
        obstaclesSpawner.Reset();
        if (Data.curScore > Data.bestScore)
        {
            Data.bestScore = Data.curScore;
            GameSocials.ReportScore((int)Data.bestScore);
        }
        score.ResetScore();
        sawsMoving.Reset();
        character.transform.position = startCharacterPos;
        character.GetComponent<Rigidbody2D>().isKinematic = false;
        Data.obtainedCoins = 0;
        coinsText.text = Data.obtainedCoins.ToString();
        Data.curScene = Data.Scene.GAME;
    }

    public void PauseGame()
    {
        Data.curScene = Data.Scene.PAUSE;
        Data.character.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Data.character.GetComponent<Rigidbody2D>().isKinematic = true;
        foreach (GameObject go in gameUiElements)
        {
            go.SetActive(false);
        }
        pauseWindow.SetActive(true);

    }

    public void ContinueGame()
    {
        Data.curScene = Data.Scene.GAME;
        //Data.character.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Data.character.GetComponent<Rigidbody2D>().isKinematic = false;
        foreach (GameObject go in gameUiElements)
        {
            go.SetActive(true);
        }
        pauseWindow.SetActive(false);

    }
}
