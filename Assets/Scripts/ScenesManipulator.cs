﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManipulator : MonoBehaviour {
    public Sprite[] ballsSprites = new Sprite[8];

    public void LoadScene(int id)
    {
        if(Data.curScene == Data.Scene.PAUSE)
        {
            if (Data.curScore > Data.bestScore)
            {
                Data.bestScore = Data.curScore;
                GameSocials.ReportScore((int)Data.bestScore);
            }
        }
        Saver.Save();
        SceneManager.LoadSceneAsync(id);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Data.curScene == Data.Scene.RESULT_WINDOW || Data.curScene == Data.Scene.SHOP) LoadScene(0);
            else if (Data.curScene == Data.Scene.MENU ) Quit();
        }
            
    }

    public void Quit()
    {
        Saver.Save();
        Application.Quit();
    }
}
