﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimation : MonoBehaviour {
    //public AudioSource audioSource; // можно вернуть
    private Vector3 startScale;

    public void PointerDown()
    {
        transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
        if (GetComponent<Animator>() != null) GetComponent<Animator>().enabled = false;
        //audioSource.Play();  // можно вернуть
    }

    public void PointerUp()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        if (GetComponent<Animator>() != null) GetComponent<Animator>().enabled = true;
    }
}
