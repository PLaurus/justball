﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public GameObject ballContainerPrefab;
    public Transform contentArea;
    public Sprite[] ballsSprites = new Sprite[8];
    public Text shopCoinsText;

    private GameObject[] shopBalls;
    private const uint START_COST = 100;
    private float costMultiplier = 1.2f;

    void Start()
    {
        Data.curScene = Data.Scene.SHOP;
        ResetCoinsText();
        if (Data.firstLaunchShop)
        {
            FirstInitialize(ballsSprites.Length);
        }
        else
        {
            SecondInitialize(ballsSprites.Length);
        }

        shopBalls = new GameObject[ballsSprites.Length];
        for (int i = 0; i < shopBalls.Length; i++)
        {
            shopBalls[i] = Instantiate(ballContainerPrefab, contentArea) as GameObject;
        }
        ResetContainers();
    }

    private void FirstInitialize(int ballsCount)
    {
        for (int i = 0; i < ballsCount; i++)
        {
            if (i != 0) costMultiplier *= 1.4f;
            Data.balls.Add(new Ball(i, (uint)(START_COST * costMultiplier)));
        }
        Data.balls[0].SetBought(true);
        Data.balls[0].selected = true;
        Data.characterSprite = ballsSprites[0];
        Data.firstLaunchShop = false;
        Saver.Save();
    }

    public void SecondInitialize(int ballsCount)
    {
        if(Data.balls.Count < ballsCount)
        {
            for (int i = Data.balls.Count; i < ballsCount; i++)
            {
                Data.balls.Add(new Ball(i, (uint)(START_COST * (i + 1) * costMultiplier)));
            }
        }////////////////////////////////////////////////
        for (int i = 0; i < Data.balls.Count; i++)
        {
            if (Data.balls[i].selected)
            {
                Data.characterSprite = ballsSprites[i];
                break;
            }
        }
        Saver.Save();
    }

    private void ResetCoinsText()
    {
        shopCoinsText.text = Data.coinsCount.ToString();
    }

    public void ChooseBall(int i)
    {
        if (Data.balls[i].IsBought())
        {
            foreach (GameObject go in shopBalls)
            {
                go.GetComponent<Animator>().enabled = false;
            }
            shopBalls[i].GetComponent<Animator>().enabled = true;

            foreach (Ball ball in Data.balls)
                ball.selected = false;
            Data.balls[i].selected = true;
            Data.coinMultiplier = Data.balls[i].coinsMultiplier;//coinsClass.ChangeMultiplier();
            Data.characterSprite = ballsSprites[i];
        }
        else
        {
            if (Data.coinsCount < Data.balls[i].cost)
            {
                Debug.Log("У вас недостаточно средств!");
            }
            else
            {
                Data.coinsCount -= Data.balls[i].cost;
                Data.balls[i].SetBought(true);
                shopCoinsText.text = Data.coinsCount.ToString();
                foreach (Ball ball in Data.balls)
                    ball.selected = false;
                Data.balls[i].selected = true;
                ResetContainers();
                ResetCoinsText();
                Data.characterSprite = ballsSprites[i];
                for(int j=0;j<Data.balls.Count;j++) {
                    if (!Data.balls[j].IsBought())
                    {
                        break;
                    }else if(j == Data.balls.Count - 1)
                    {
                        GameSocials.UnlockAchivement(GameSocials.achievement9);
                    }
                }
            }

        }
        Saver.Save();
    }

    private void ResetContainers()
    {
        for (int i = 0; i < shopBalls.Length; i++)
        {
            BallContainer ballContainer = shopBalls[i].GetComponent<BallContainer>();
            ballContainer.ballImage.sprite = ballsSprites[i];
            ballContainer.SetId(i);

            if(Data.balls[i].coinsMultiplier > 1)
            {
                Data.coinMultiplier = Data.balls[i].coinsMultiplier;
                ballContainer.coinsMultContainer.SetActive(true);
                ballContainer.coinsMultText.text = "x"+ Data.coinMultiplier.ToString();
            }else
            {
                ballContainer.coinsMultContainer.SetActive(false);
            }

            if (Data.balls[i].forceMultiplier > 1)
            {
                Data.forceMultiplier = Data.balls[i].forceMultiplier;
                ballContainer.forceMultContainer.SetActive(true);
                ballContainer.forceMultText.text = "x" + Data.forceMultiplier.ToString();
            }
            else
            {
                ballContainer.forceMultContainer.SetActive(false);
            }

            if (Data.balls[i].IsBought())
            {
                ballContainer.lockGO.SetActive(false);
                ballContainer.costBackground.SetActive(false);
                if (Data.balls[i].selected)
                {
                    shopBalls[i].GetComponent<Animator>().enabled = true;
                }
                else
                {
                    shopBalls[i].GetComponent<Animator>().enabled = false;
                }
            }
            else
            {
                ballContainer.lockGO.SetActive(true);
                ballContainer.costBackground.SetActive(true);
                ballContainer.cost.text = Data.balls[i].cost.ToString();
                shopBalls[i].GetComponent<Animator>().enabled = false;
            }
        }
    }

}

[Serializable]
public class Ball
{
    public int id { get; private set; }
    public uint cost { get; private set; }
    public int coinsMultiplier { get; private set; }
    public float forceMultiplier { get; private set; }
    private bool isBought;
    public bool selected = false;
    public Ball(int id, uint cost)
    {
        this.id = id;
        this.cost = cost;
        isBought = false;
        SetMultipier(id);
    }

    public void SetBought(bool isBought)
    {
        this.isBought = isBought;
    }

    private void SetMultipier(int id)
    {
        coinsMultiplier = id / 4 + 1;
        if ((id + 1) % 4 == 0)
        {
            forceMultiplier = 1.4f;
        }
        else if ((id + 1) % 4 == 3)
        {
            forceMultiplier = 1.3f;
        }
        else if ((id + 1) % 4 == 2)
        {
            forceMultiplier = 1.2f;
        }
        else
        {
            forceMultiplier = 1.0f;
        }
        
        /*switch (id)
        {
            case 1:
                coinsMultiplier = 2;
                break;
            case 2:
                coinsMultiplier = 3;
                break;
            case 3:
                coinsMultiplier = 4;
                break;
        }*/
    }

    public bool IsBought()
    {
        return isBought;
    }

    public void SetCost(uint cost)
    {
        this.cost = cost;
    }

    public void SetId(int id)
    {
        this.id = id;
    }
}

