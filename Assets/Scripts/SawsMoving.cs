﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawsMoving : MonoBehaviour {

    private const float START_SPEED = 0.9f;//3,3
    private const float FINAL_SPEED = 0.95f;
    private const uint CONST_UP = 2;//multiplier
    private float ADDITIONAL_SPEED = 0.53f;
    private const uint nextSU = 5;
    private uint nextSpeedUp = 5;//score
    private float speed;
    private Vector3 startPos;

    void Start()
    {
        ADDITIONAL_SPEED = (FINAL_SPEED - START_SPEED) / 6.0f;
        startPos = transform.position;
        speed = START_SPEED;
    }

    void Update()
    {
        if (Data.curScene == Data.Scene.GAME)
        {
            ChangeSpeed();
            transform.position -= new Vector3(0f, speed * Time.deltaTime);
        }
    }

    public float GetStartSpeed()
    {
        return START_SPEED;
    }

    public float GetFinalSpeed()
    {
        return FINAL_SPEED;
    }

    void ChangeSpeed()
    {
        if (speed < FINAL_SPEED)
        {
            if (Data.curScore >= nextSpeedUp)
            {
                nextSpeedUp *= CONST_UP;
                speed += ADDITIONAL_SPEED;
                //characterMove.IncreaseSpeed();
            }
        } 
    }

    public void Reset()
    {
        transform.position = startPos;
        speed = START_SPEED;
        nextSpeedUp = nextSU;
        //characterMove.ResetSpeed();
    }
}
