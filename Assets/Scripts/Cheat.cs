﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cheat : MonoBehaviour {
    public uint additionalMoney = 5;
    public Text coins;

    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            Data.coinsCount -= additionalMoney;
            coins.text = Data.coinsCount.ToString();
        }
        else if (Input.GetKey(KeyCode.Q))
        {
            Data.coinsCount += additionalMoney;
            coins.text = Data.coinsCount.ToString();
        }
    }
}
