﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Data{
    //public static Score
    public static Transform character;
    public static uint curScore = 0;
    public static uint bestScore = 0;
    public static bool sound = false;
    public static uint coinsCount = 0;
    public static uint obtainedCoins = 0;
    public static int coinMultiplier = 1;
    public static float forceMultiplier = 1f;
    public static List<Ball> balls = new List<Ball>();
    public static bool firstLaunchMenu = true;
    public static bool firstLaunchShop = true;
    public static DateTime rewardPreviousDay;
    public static uint todayCoinsBonus = 0;
    public static Sprite characterSprite;
    public static bool allGameStarted = false;
    

    public enum Scene
    {
        MENU,
        GAME,
        SHOP,
        RESULT_WINDOW,
        EXIT,
        PAUSE,
    }

    public static Scene curScene = Scene.GAME;


}
