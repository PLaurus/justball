﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosController : MonoBehaviour {
    public Score score;

    private Vector3 startPos;

    private Vector3 bottomCheckPoint;
    private Vector3 upperCheckPoint;

    void Start()
    {
        ResetCheckPoints();
        startPos = transform.position;
    }

    void Update()
    {

        if (Data.curScene == Data.Scene.GAME)
        {
            if (Data.character.position.y < bottomCheckPoint.y && Data.character.GetComponent<Rigidbody2D>().velocity.y < 0f){
                //transform.position += new Vector3(0f, Data.character.GetComponent<Rigidbody2D>().velocity.y * Time.deltaTime, 0f);
                transform.position -= new Vector3(0f, bottomCheckPoint.y - Data.character.position.y, 0f);
                ResetCheckPoints();
                score.UpdateScore();
            }
            else if (Data.character.position.y > upperCheckPoint.y && Data.character.GetComponent<Rigidbody2D>().velocity.y > 0f)
            {
                //transform.position += new Vector3(0f, Data.character.GetComponent<Rigidbody2D>().velocity.y * Time.deltaTime, 0f);
                transform.position += new Vector3(0f, Data.character.position.y - upperCheckPoint.y, 0f);
                ResetCheckPoints();
            }
        }
    }

    public void ResetCameraPos()
    {
        transform.position = startPos;
    }

    public void ResetCheckPoints()
    {
        setCheckPoints();
    }

    public void setCheckPoints()
    {
        bottomCheckPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height * 0.5f, 0f));
        
        upperCheckPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height * 3 / 4, 0f));
        Vector3 start = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * 1 / 4, 0f));//
        Debug.DrawLine(start, upperCheckPoint);
    }
}
